using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class playerchk : MonoBehaviour
{
    public static bool gameover;
    public GameObject gameoverscreen;
    public static bool gamestarted;
    public GameObject startingtext;
    public static int numbercoins;
    public Text score;
    public static bool gonext;
    public static GameObject gonextscreen;


    // Start is called before the first frame update
    void Start()
    {
        gameover = false;
        Time.timeScale = 1;
        gamestarted = false;
        //numbercoins = 0;
        gonext = false;

    }

    // Update is called once per frame
    void Update()
    {
        if (gameover)
        {
            Time.timeScale = 0;
            gameoverscreen.SetActive(true);
        }
        score.text = "score =" + numbercoins;
        if (mouse.tap)
        {
            gamestarted = true;
            Destroy(startingtext);
        }




    }


}
