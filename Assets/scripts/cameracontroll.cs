using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameracontroll : MonoBehaviour
{
    public Transform player;
    private Vector3 cam;
    // Start is called before the first frame update

    void Start()
    {
        cam = transform.position - player.position;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 pos1 = new Vector3(transform.position.x, transform.position.y, cam.z + player.position.z);
        transform.position = pos1;
    }
}
