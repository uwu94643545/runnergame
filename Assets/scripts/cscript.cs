using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class levelupscript : MonoBehaviour
{
    public GameObject levelupscreen;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (playerchk.numbercoins > 5)
        {
            pausegame();
        }


    }
    public void pausegame()
    {
        Time.timeScale = 0;
        levelupscreen.SetActive(true);

    }
    public void resumegame()
    {
        Time.timeScale = 1;
        levelupscreen.SetActive(false);


    }
}