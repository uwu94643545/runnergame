using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class roadcreate : MonoBehaviour
{
    public GameObject[] roadprefabs;
    public float spawn = 0;
    public float roadlength = 30;
    public int numberofraods = 5;
    private List<GameObject> activeroads = new List<GameObject>();
    public Transform playertrsfrm;
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < numberofraods; i++)
        {
            spawnroad(Random.Range(0, roadprefabs.Length));
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (playertrsfrm.position.z > spawn - (numberofraods * roadlength))
        {
            spawnroad(Random.Range(0, roadprefabs.Length));
            deleteroad();
        }
    }
    public void spawnroad(int roadindex)
    {
        GameObject add = Instantiate(roadprefabs[roadindex], transform.forward * spawn, transform.rotation);
        activeroads.Add(add);
        spawn += roadlength;
    }
    public void deleteroad()
    {
        Destroy(activeroads[0]);
        activeroads.RemoveAt(0);
    }
}
