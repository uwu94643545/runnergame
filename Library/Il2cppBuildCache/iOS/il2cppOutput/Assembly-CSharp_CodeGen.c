﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void cameracontroll::Start()
extern void cameracontroll_Start_m7319F66BB4A5ED34D3FD06EE5E89C6B050345C1F (void);
// 0x00000002 System.Void cameracontroll::Update()
extern void cameracontroll_Update_m60D77716C23AC943FD54657075DDB69CC4594719 (void);
// 0x00000003 System.Void cameracontroll::.ctor()
extern void cameracontroll__ctor_mEADF2568610593BA52AD74CA07D93F613201C0C7 (void);
// 0x00000004 System.Void click::replay()
extern void click_replay_mA75BFAB50BBD33E96A1CBE6B3A3872782773E287 (void);
// 0x00000005 System.Void click::.ctor()
extern void click__ctor_m0D8FA1C1CF383BEC09A4B55349391B6C3DFF7C26 (void);
// 0x00000006 System.Void coinscrpt::Start()
extern void coinscrpt_Start_mA68941F01D47A7BEE77F85AB03C1A12D960679AB (void);
// 0x00000007 System.Void coinscrpt::Update()
extern void coinscrpt_Update_m6210F840E0540D7BAC44A186475CBEDBA29BE872 (void);
// 0x00000008 System.Void coinscrpt::OnTriggerEnter(UnityEngine.Collider)
extern void coinscrpt_OnTriggerEnter_m0B41776EFB3F4C9D39A5E7ABC15A002D38EF1CC4 (void);
// 0x00000009 System.Void coinscrpt::.ctor()
extern void coinscrpt__ctor_m8CE004EDFF05618C1BD174C4222F42C71FD83896 (void);
// 0x0000000A System.Void levelupscript::Start()
extern void levelupscript_Start_m65D3393FDFD8485DBD6855544A01CC84424E2441 (void);
// 0x0000000B System.Void levelupscript::Update()
extern void levelupscript_Update_m3F4F5BFB4CFFB4304A2EDBA03A843493F4FF3263 (void);
// 0x0000000C System.Void levelupscript::pausegame()
extern void levelupscript_pausegame_m8C3B3F264538DBBB5C86316A40F4A3BE2FCD88B5 (void);
// 0x0000000D System.Void levelupscript::resumegame()
extern void levelupscript_resumegame_m23B098A9A530C558FA33ECA3992E38DF4E37CFB1 (void);
// 0x0000000E System.Void levelupscript::.ctor()
extern void levelupscript__ctor_m4A2A42E9E73224EB80AD6BEDCE7B8688288F15DA (void);
// 0x0000000F System.Void mouse::Update()
extern void mouse_Update_m207C39033E3891DF4A3BAEE92A1DC6FA7429EE18 (void);
// 0x00000010 System.Void mouse::Reset()
extern void mouse_Reset_m5ED485E97C06C26B689073FC57C047DE25577B08 (void);
// 0x00000011 System.Void mouse::.ctor()
extern void mouse__ctor_m8F8C8E9ADAD9DC637D3579BAA13EDF88E166489A (void);
// 0x00000012 System.Void pausemanager::Start()
extern void pausemanager_Start_mC923CF8C5D30E00CC6046E972AEAA012417419E6 (void);
// 0x00000013 System.Void pausemanager::Update()
extern void pausemanager_Update_mFC38837CFE820B83010B48F1B10AEB73449D7056 (void);
// 0x00000014 System.Void pausemanager::pausegame()
extern void pausemanager_pausegame_mD70F4BFA9F75AFFAB8B9BF0D27625CE3C035E9DE (void);
// 0x00000015 System.Void pausemanager::resumegame()
extern void pausemanager_resumegame_m82F7ECD8EA51D662A4F07E69BA5C16D01922DE5E (void);
// 0x00000016 System.Void pausemanager::.ctor()
extern void pausemanager__ctor_m721B5A495C53883B37DC24B42C38C2A30E35C98B (void);
// 0x00000017 System.Void playerchk::Start()
extern void playerchk_Start_mB2B04141ED1E33E24B413D1847DA3CB5ABE74F1C (void);
// 0x00000018 System.Void playerchk::Update()
extern void playerchk_Update_mCFE6A22B6B6B80576888FAF98F11E0C5D84C9979 (void);
// 0x00000019 System.Void playerchk::.ctor()
extern void playerchk__ctor_mDD98FA0BC05A320FAA8212700994532D8CF2022C (void);
// 0x0000001A System.Void playercontroll::Start()
extern void playercontroll_Start_m256209E89894193F4E73B035293FE4C24DC8E294 (void);
// 0x0000001B System.Void playercontroll::Update()
extern void playercontroll_Update_m23DD7902DBFB8680ABE1C5849EDE901353EC488B (void);
// 0x0000001C System.Void playercontroll::FixedUpdate()
extern void playercontroll_FixedUpdate_m50088CDAB6A1B671AB14E545F10BB56F8DA9776C (void);
// 0x0000001D System.Void playercontroll::OnControllerColliderHit(UnityEngine.ControllerColliderHit)
extern void playercontroll_OnControllerColliderHit_m426B767D1E539F95320992066DE091A36D4577BF (void);
// 0x0000001E System.Void playercontroll::.ctor()
extern void playercontroll__ctor_m9F7CA49B0F02B078F8163A07C356A1B44D86EA93 (void);
// 0x0000001F System.Void playerdata::Update()
extern void playerdata_Update_m39285DFED11D75F198783ACD6AA5E2B981A9F7D5 (void);
// 0x00000020 System.Void playerdata::save()
extern void playerdata_save_mDB45763C0BDD43378763CD888D935FBC2392DDDD (void);
// 0x00000021 System.Void playerdata::load()
extern void playerdata_load_m32461479D1AED8D64B7C07650D64AF60A8787D0A (void);
// 0x00000022 System.Void playerdata::.ctor()
extern void playerdata__ctor_mE3F55F985E28755F060FCEF9F893DDF50EBAE400 (void);
// 0x00000023 System.Void roadcreate::Start()
extern void roadcreate_Start_m321506ECF7B609CFFDEA5763361D6E7EDE764CDB (void);
// 0x00000024 System.Void roadcreate::Update()
extern void roadcreate_Update_m641B9A2B4EC33518AB0E66C5750F0771230E66B2 (void);
// 0x00000025 System.Void roadcreate::spawnroad(System.Int32)
extern void roadcreate_spawnroad_m5989915713D16884FF4FC1A4167A2DB185E81B91 (void);
// 0x00000026 System.Void roadcreate::deleteroad()
extern void roadcreate_deleteroad_mD0E34814BD12A27175E6D67C439E11F8530B2DCB (void);
// 0x00000027 System.Void roadcreate::.ctor()
extern void roadcreate__ctor_m67EB62155E1BC55FFCB95B9ECB1CA18E1841249D (void);
static Il2CppMethodPointer s_methodPointers[39] = 
{
	cameracontroll_Start_m7319F66BB4A5ED34D3FD06EE5E89C6B050345C1F,
	cameracontroll_Update_m60D77716C23AC943FD54657075DDB69CC4594719,
	cameracontroll__ctor_mEADF2568610593BA52AD74CA07D93F613201C0C7,
	click_replay_mA75BFAB50BBD33E96A1CBE6B3A3872782773E287,
	click__ctor_m0D8FA1C1CF383BEC09A4B55349391B6C3DFF7C26,
	coinscrpt_Start_mA68941F01D47A7BEE77F85AB03C1A12D960679AB,
	coinscrpt_Update_m6210F840E0540D7BAC44A186475CBEDBA29BE872,
	coinscrpt_OnTriggerEnter_m0B41776EFB3F4C9D39A5E7ABC15A002D38EF1CC4,
	coinscrpt__ctor_m8CE004EDFF05618C1BD174C4222F42C71FD83896,
	levelupscript_Start_m65D3393FDFD8485DBD6855544A01CC84424E2441,
	levelupscript_Update_m3F4F5BFB4CFFB4304A2EDBA03A843493F4FF3263,
	levelupscript_pausegame_m8C3B3F264538DBBB5C86316A40F4A3BE2FCD88B5,
	levelupscript_resumegame_m23B098A9A530C558FA33ECA3992E38DF4E37CFB1,
	levelupscript__ctor_m4A2A42E9E73224EB80AD6BEDCE7B8688288F15DA,
	mouse_Update_m207C39033E3891DF4A3BAEE92A1DC6FA7429EE18,
	mouse_Reset_m5ED485E97C06C26B689073FC57C047DE25577B08,
	mouse__ctor_m8F8C8E9ADAD9DC637D3579BAA13EDF88E166489A,
	pausemanager_Start_mC923CF8C5D30E00CC6046E972AEAA012417419E6,
	pausemanager_Update_mFC38837CFE820B83010B48F1B10AEB73449D7056,
	pausemanager_pausegame_mD70F4BFA9F75AFFAB8B9BF0D27625CE3C035E9DE,
	pausemanager_resumegame_m82F7ECD8EA51D662A4F07E69BA5C16D01922DE5E,
	pausemanager__ctor_m721B5A495C53883B37DC24B42C38C2A30E35C98B,
	playerchk_Start_mB2B04141ED1E33E24B413D1847DA3CB5ABE74F1C,
	playerchk_Update_mCFE6A22B6B6B80576888FAF98F11E0C5D84C9979,
	playerchk__ctor_mDD98FA0BC05A320FAA8212700994532D8CF2022C,
	playercontroll_Start_m256209E89894193F4E73B035293FE4C24DC8E294,
	playercontroll_Update_m23DD7902DBFB8680ABE1C5849EDE901353EC488B,
	playercontroll_FixedUpdate_m50088CDAB6A1B671AB14E545F10BB56F8DA9776C,
	playercontroll_OnControllerColliderHit_m426B767D1E539F95320992066DE091A36D4577BF,
	playercontroll__ctor_m9F7CA49B0F02B078F8163A07C356A1B44D86EA93,
	playerdata_Update_m39285DFED11D75F198783ACD6AA5E2B981A9F7D5,
	playerdata_save_mDB45763C0BDD43378763CD888D935FBC2392DDDD,
	playerdata_load_m32461479D1AED8D64B7C07650D64AF60A8787D0A,
	playerdata__ctor_mE3F55F985E28755F060FCEF9F893DDF50EBAE400,
	roadcreate_Start_m321506ECF7B609CFFDEA5763361D6E7EDE764CDB,
	roadcreate_Update_m641B9A2B4EC33518AB0E66C5750F0771230E66B2,
	roadcreate_spawnroad_m5989915713D16884FF4FC1A4167A2DB185E81B91,
	roadcreate_deleteroad_mD0E34814BD12A27175E6D67C439E11F8530B2DCB,
	roadcreate__ctor_m67EB62155E1BC55FFCB95B9ECB1CA18E1841249D,
};
static const int32_t s_InvokerIndices[39] = 
{
	1171,
	1171,
	1171,
	1171,
	1171,
	1171,
	1171,
	1000,
	1171,
	1171,
	1171,
	1171,
	1171,
	1171,
	1171,
	1171,
	1171,
	1171,
	1171,
	1171,
	1171,
	1171,
	1171,
	1171,
	1171,
	1171,
	1171,
	1171,
	1000,
	1171,
	1171,
	1171,
	1171,
	1171,
	1171,
	1171,
	991,
	1171,
	1171,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	39,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
